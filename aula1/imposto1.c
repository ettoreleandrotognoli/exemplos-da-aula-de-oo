#include <stdio.h>


int main(int argc,char ** argv){

	char name[100];
	char nickname[50];
	char momName[100];
	float monthlyPay;
	float tax = 0.11f;
	char response;

	do{
		printf("Qual é seu nome?\n");
		scanf("%s",name);
		printf("Qual é seu apelido?\n");
		scanf("%s",nickname);
		printf("Qual o nome da sua mãe?\n");
		scanf("%s",momName);
		printf("Qual seu salário mensal?\n");
		scanf("%f",&monthlyPay);
		printf("Então você se chama %s, sua mãe se chama %s, mas posso lhe chamar de %s?\n",name,momName,nickname);	
		scanf(" %c",&response);
	}
	while(response != 's');
	
	printf("Você recebe R$%.2f ao ano\n",monthlyPay*12.0f);
	printf("Desconto de R$%.2f de imposto\n",monthlyPay*12.0f*tax);

}