<?php

interface HtmlElement{

	public function getTag();
	public function isTag($tag);
	public function getId();
	public function setId($id);
	public function getClasses();
	public function hasClass($className);
	public function getAttributes();
	public function setAttributes(Array $attributes);
	public function hasAttribute($attr);
	public function getValue($attr);
	public function render();
	public function getContent();
	public function setContent($content);

};

abstract class AbstractHtmlElement implements HtmlElement{

	protected $tag;
	protected $attributes;

	public function AbstractHtmlElement($tag,$attributes=array()){
		$this->tag = $tag;
		$this->attributes = $attributes;
	}

	public function getTag(){
		return $this->tag;
	}

	public function isTag($tag){
		return $this->tag == $tag;
	}

	public function getId(){
		if(array_key_exists('id',$this->attributes)){
			return $this->attributes['id'];
		}
		return null;
	}

	public function setId($id){
		$this->attributes['id'] = $id;
	}

	public function getClasses(){
		if(array_key_exists('class',$this->attributes)){
			return preg_split('@ +@',$this->attributes['class'],-1,PREG_SPLIT_NO_EMPTY);
		}
		return array();
	}

	public function hasClass($className){
		if(!array_key_exists('class',$this->attributes)){
			return false;
		}
		return strpos($this->attributes['class'],$className) ==! false;
	}

	public function getAttributes(){
		return $this->attributes;
	}

	public function setAttributes(Array $attributes){
		$this->attributes = $attributes;
	}

	public function hasAttribute($attr){
		return array_key_exists($attr,$this->attributes);
	}

	public function getValue($attr){
		if(!array_key_exists($attr,$this->attributes)){
			return null;
		}
		return $this->attributes[$attr];
	}

	public abstract function render();

};

class DefaultElement extends AbstractHtmlElement{

	private $content;

	public function __construct($tag,$attributes=array(),$content=array()){
		parent::__construct($tag,$attributes);
		$this->content = $content;
	}

	public function setContent($content){
		$this->content = $content;
	}

	public function getContent(){
		return $this->content;
	}

	public function render(){
		$attr = array();
		foreach ($this->attributes as $key => $value) {
			$attr[] = $key.'="'.$value.'"';
		}
		echo '<',$this->tag,' ',implode(' ',$attr),'>';
		$content = $this->content;
		if(!is_array($content)){
			$content = array($content);
		}
		foreach ($content as $innerElement) {
			if(is_string($innerElement)){
				echo htmlentities($innerElement);
				continue;
			}
			if($innerElement instanceof HtmlElement){
				$innerElement->render();
			}
		}
		echo '</',$this->tag,'>';
	}

};

class SelfClosingElement extends AbstractHtmlElement{

	public function __construct($tag,$attributes=array()){
		parent::__construct($tag,$attributes);
	}

	public function setContent($content){
		throw new Exception();
	}

	public function getContent(){
		return null;
	}

	public function render(){
		$attr = array();
		foreach ($this->attributes as $key => $value) {
			$attr[] = $key.'="'.$value.'"';
		}
		echo '<',$this->tag,' ',implode(' ',$attr),'/>';
	}

};

class Html {

	private static $selfClosing = array(
		'meta',
		'img',
		'input'
	);

	public static function __callStatic($methodName,$args){
		if(in_array($methodName,self::$selfClosing)){
			$htmlElement = new SelfClosingElement($methodName);
		}
		else{
			$htmlElement = new DefaultElement($methodName);	
		}
		if(empty($args)){
			return $htmlElement;
		}
		$attributes = array_shift($args);
		if(is_string($attributes)){
			$htmlElement->setId($attributes);
		}
		else{
			$htmlElement->setAttributes($attributes);
		}
		if(empty($args)){
			return $htmlElement;
		}
		if(is_array($args[0])){
			$htmlElement->setContent($args[0]);
			return $htmlElement;
		}
		$htmlElement->setContent($args);
		return $htmlElement;
	}

};

Html::html()->render();	
Html::meta()->render();	
Html::img('load-img')->render();
Html::div(array('class'=>'span2'),Html::label(array(),'Nome:'),Html::input(array('type'=>'text','name'=>'name')))->render();

Html::html(array(),
	Html::head(array(),
		Html::meta(),
		Html::title(array(),'Titulo da Pagina')
	),
	Html::body(array(),
		Html::div('head',
			Html::div('panel','Menu Superior')
		),
		Html::div('center'),
		Html::div('foot',
			Html::div('panel','Rodape')	
		)
	)
)->render();