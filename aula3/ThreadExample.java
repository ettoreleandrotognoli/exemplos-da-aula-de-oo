import java.lang.Runnable;
import java.lang.Thread;

public class ThreadExample implements Runnable{

	@Override
	public void run(){
		System.out.println("RUN!!!");
	}

	public static void main(String... args){

		new Thread(new ThreadExample()).start();

		new Thread(new Runnable(){
			@Override
			public void run(){
				System.out.println("RUN!!! (Classe Anonima)");
			}
		}).start();

		new Thread(new ThreadExample(){
			@Override
			public void run(){
				System.out.println("RUN!!! (Outra Classe Anonima)");
			}
		}).start();
	}
}
