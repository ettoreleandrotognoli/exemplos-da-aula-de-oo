class GetAndSet(object):

	def __init__(self):
		self.attribute = ""


if __name__ == "__main__":
	getAndSet = GetAndSet()

	getAndSet.attribute = "Ettore"
	print getAndSet.attribute

	getAndSet.attribute += " Leandro"
	print getAndSet.attribute

	getAndSet.attribute += " Tognoli"
	print getAndSet.attribute

	getAndSet.attribute = ""
	print getAndSet.attribute

	getAndSet.attribute = None
	print getAndSet.attribute
