#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

class Notificador{
public:
	virtual void notificar(string usuario,string mensagem) = 0;
};


class NotificadorSMS : Notificador{
public:
	
	void notificar(string usuario,string mensagem){
		system("speaker-test -t sine -f 1000 -p 100 -l 1");
		cout << endl;
		cout << "\a" << (char)7 << endl;
		cout << " __         __ " << endl;
		cout << "|__| ))))) |  |" << endl;
		cout << "/##\\       |  |" << endl;
		cout << "           |__|" << endl;
		cout << endl;
		cout << "SMS para : "<< usuario << endl;
		cout << mensagem << endl;
		cout << endl;
	}

};

class NotificadorEmail : Notificador{
public:

	void notificar(string usuario,string mensagem){
		ofstream outFile("email.txt",ofstream::app);
		outFile << "Email: " << usuario << endl;
		outFile << "Message: " << mensagem << endl << endl;
		outFile.close();
	}	

};

class NotificadorComunicado : Notificador{
public:

	void notificar(string usuario,string mensagem){
		cout << "<div class=\"message\"><p>"<<mensagem<<"</p><hr /><small>To:\""<<usuario<<"\"</small></div>" << endl;
	}	

};



int main(int argc,char ** argv){
	list<Notificador*> notificadores;
	notificadores.push_back((Notificador*)new NotificadorComunicado());
	notificadores.push_back((Notificador*)new NotificadorEmail());
	notificadores.push_back((Notificador*)new NotificadorSMS());
	string usuario;
	string mensagem;
	cout << "Quem você deseja notificar?" << endl;
	cin >> usuario;
	cout << "Qual é a mensagem?" << endl;
	cin >> mensagem;	
	cout << "Enviando Notificação..." << endl;
	for(list<Notificador*>::iterator it = notificadores.begin(); it != notificadores.end();it++){
		Notificador * notificador = *it;
		notificador->notificar(usuario,mensagem);
	}
	cout << "Enviado com Sucesso!!!" << endl;
	for(list<Notificador*>::iterator it = notificadores.begin(); it != notificadores.end();it++){
		Notificador * notificador = *it;
		delete notificador;
	}
}
