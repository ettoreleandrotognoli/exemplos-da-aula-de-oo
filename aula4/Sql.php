<?php

class Sql {

	public static function makeInsert($table,$value,$returning=''){
		$columns = array_keys($value);
		$columns = implode('","',$columns);
		$values = implode(',',array_fill(0,count($value),'?'));
		if(!empty($returning)){
			$returning = ' RETURNING "'.$returning.'"';
		}
		$sql = 'INSERT INTO "'.$table.'"("'.$columns.'") VALUES ('.$values.')'.$returning.';';
		return array($sql,array_values($value));
	}

}