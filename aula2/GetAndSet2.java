public class GetAndSet2{

	private Object attribute;

	public void setAttribute(Object value) throws Exception{
		if(value == null)
			throw new Exception("Attribute nao deve ser nulo");
		this.attribute = value;
		System.out.println(this.attribute);
	}

	public Object getAttribute(){
		return this.attribute;
	}


	public static void main(String... args) throws Exception{
		GetAndSet2 getAndSet = new GetAndSet2();

		getAndSet.setAttribute("Ettore");

		getAndSet.setAttribute(getAndSet.getAttribute() + " Leandro");

		getAndSet.setAttribute(getAndSet.getAttribute() + " Tognoli");

		getAndSet.setAttribute("");

		getAndSet.setAttribute(null);
	}

};