#ifndef __PERSON_HPP__
#define __PERSON_HPP__

class Person{
public:
	
char name[100];
char nickname[50];
char momName[100];
float monthlyPay;

void read(){
	printf("Qual é seu nome?\n");
	scanf("%s",this->name);
	printf("Qual é seu apelido?\n");
	scanf("%s",this->nickname);
	printf("Qual o nome da sua mãe?\n");
	scanf("%s",this->momName);
	printf("Qual seu salário mensal?\n");
	scanf("%f",&(this->monthlyPay));
}

void readAndConfirm(){
	char response;
	do{
		this->read();
		printf("Então você se chama %s, sua mãe se chama %s, mas posso lhe chamar de %s\n",this->name,this->momName,this->nickname);		
		scanf(" %c",&response);
	}
	while(response != 's');
}

void print(float tax){
	printf("Você recebe R$%.2f ao ano\n",this->monthlyPay*12.0f);
	printf("Desconto de R$%.2f de imposto\n",this->monthlyPay*12.0f*tax);
}

void writeJSON(FILE * file){
	fprintf(file,"{name:\"%s\",nickname:\"%s\",momName:\"%s\",monthlyPay:%f}\n",this->name,this->nickname,this->momName,this->monthlyPay);
}

};

#endif

