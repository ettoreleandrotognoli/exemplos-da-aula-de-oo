public class GetAndSet{

	public Object attribute;


	public static void main(String... args){
		GetAndSet getAndSet = new GetAndSet();

		getAndSet.attribute = "Ettore";
		System.out.println(getAndSet.attribute);

		getAndSet.attribute += " Leandro";
		System.out.println(getAndSet.attribute);

		getAndSet.attribute += " Tognoli";
		System.out.println(getAndSet.attribute);

		getAndSet.attribute = "";
		System.out.println(getAndSet.attribute);

		getAndSet.attribute = null;
		System.out.println(getAndSet.attribute);
	}

};