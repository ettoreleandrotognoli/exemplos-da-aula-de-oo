<?php

class Entrada {

	private $file;

	public function __construct($inputFile){
		$this->file = fopen($inputFile,'r');
	}

	public function __destruct(){
		fclose($this->file);
	}

	/**
	* Deve ler uma linha do arquivo
	* e fazer um parse para array
	* array chave => valor
	*/
	public function getNext(){
		//...
	}

	/**
	* retorna false se encontrou o fim do arquvio
	*/
	public function hasNext(){
		//...
	}

}

class Saida {

	private $file;

	public function __construct($outputFile){
		$this->file = fopen($outputFile,'w');
	}

	public function __destruct(){
		fclose($this->file);
	}

	/**
	* Deve receber um array php
	* e escrever uma linha no arquivo
	*/
	public function putNext(Array $line){
		//...
	}

}

class Conversor{

	public $entrada;
	public $saida;

	public function __construct(Entrada $entrada,Saida $saida){
		$this->entrada = $entrada;
		$this->saida = $saida;
	}

	public function convert(){
		while($this->entrada->hasNext()){
			$next = $this->entrada->getNext();
			$this->saida->putNext($next);
		}
	}

}

$entrada = new Entrada("table.txt");
$saida = new Saida("table.xml");
$conversor = new Conversor($entrada,$saida);
$conversor->convert();
