<?php

class SqlTest extends PHPUnit_Framework_TestCase {


	public function testClassExists(){
		$this->assertTrue(class_exists('Sql'));
	}

	public function testInsert(){
		$toInsert = array(
			'nome' => 'Éttore',
			'sexo' => 'M'
		);
		$table = 'pessoa';
		$insert = Sql::makeInsert($table,$toInsert);
		$expected = array(
			'INSERT INTO "pessoa"("nome","sexo") VALUES (?,?);',
			array('Éttore','M')
		);
		$this->assertEquals($expected,$insert);
	}

	public function testInsertWithReturn(){
		$toInsert = array(
			'nome' => 'Éttore',
			'sexo' => 'M'
		);
		$table = 'pessoa';
		$insert = Sql::makeInsert($table,$toInsert,'id');
		$expected = array(
			'INSERT INTO "pessoa"("nome","sexo") VALUES (?,?) RETURNING "id";',
			array('Éttore','M')
		);
		$this->assertEquals($expected,$insert);
	}

}