

class ProtectionTest {
private:
	int privateAttribute;
protected:
	int protectedAttribute;
public:

	int publicAttribute;

	void setPrivateAttribute(int value){
		this->privateAttribute = value;
	}

	void setProtectedAttribute(int value){
		this->protectedAttribute = value;
	}

};

class ChildClass : ProtectionTest{
public:

	//erro de compilação
	/*void tryAccessPrivateAttribute(){
		ProtectionTest::privateAttribute = 1;
	}*/

	void tryAccessProtectedAttribute(){
		ProtectionTest::protectedAttribute = 1;
	}

};


int main(int argc, char ** argv){
	ProtectionTest test;

	//OK
	test.publicAttribute = 1;

	//erro de compilação
	//test.protectedAttribute = 1;
	//OK
	test.setProtectedAttribute(1);
	
	//erro de compilação
	//test.privateAttribute = 1;
	//OK
	test.setPrivateAttribute(1);
	
}