class GetAndSet(object):

	def __init__(self):
		self._attribute = ""

	def setAttribute(self,value):
		if(value == None):
			raise Exception("attribute nao pode ser nulo")
		self._attribute = value;
		print self._attribute

	def getAttribute(self):
		return self._attribute;

	attribute = property(getAttribute,setAttribute)


if __name__ == "__main__":
	
	getAndSet = GetAndSet()

	getAndSet.attribute = "Ettore"

	getAndSet.attribute += " Leandro"

	getAndSet.attribute += " Tognoli"

	getAndSet.attribute = ""

	getAndSet.attribute = None
