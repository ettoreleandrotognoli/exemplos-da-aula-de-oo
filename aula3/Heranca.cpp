#include <iostream>

class Animal {
public:
	Animal(){
		std::cout << "Animal()"  << std::endl;
	}
	virtual ~Animal(){
		std::cout << "~Animal()" << std::endl;
	}
	virtual void fazerBarulho() =0;
};

class Lobo : virtual Animal {
public:
	Lobo(){
		std::cout << "Lobo()"<< std::endl;
	}
	~Lobo(){
		std::cout << "~Lobo()"<< std::endl;
	}
	void fazerBarulho(){
		std::cout << "Auuuuuu!!!" << std::endl;
	};
};

class Pessoa : virtual Animal {
public:
	Pessoa(){
		std::cout << "Pessoa()" << std::endl;
	}
	~Pessoa(){
		std::cout << "~Pessoa()" << std::endl;
	}
	void fazerBarulho(){	
		std::cout << "Olá" << std::endl;
	};
};

class Lobisomen : Pessoa , Lobo {
public:
	Lobisomen(){
		std::cout << "Lobisomen()" << std::endl;
	}
	~Lobisomen(){
		std::cout << "~Lobisomen()" << std::endl;
	}
	void fazerBarulho(){
		Lobo::fazerBarulho();
	};
};

class Metamorfico : Animal {
public:
	Animal * estado;
	Metamorfico(){
		std::cout << "Metamorfico()" << std::endl;
	}
	~Metamorfico(){
		std::cout << "~Metamorfico()" << std::endl;
	}
	void fazerBarulho(){
		this->estado->fazerBarulho();
	}
};


int main(int argc,char ** argv){
	{
		Lobisomen lobisomen;
		lobisomen.fazerBarulho();	
	}
	{
		Pessoa pessoa;
		Lobo lobo;
		Metamorfico metamorfico;
		metamorfico.estado = (Animal*)&pessoa;
		metamorfico.fazerBarulho();
		metamorfico.estado = (Animal*)&lobo;
		metamorfico.fazerBarulho();	
	}
}

