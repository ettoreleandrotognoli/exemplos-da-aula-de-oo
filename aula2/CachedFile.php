<?php

class CachedFile {

	private $fileName;
	private $modificationTime;

	private $values;

	public function __construct($fileName){
		$this->fileName = $fileName;
		$this->modificationTime = filemtime($fileName);
		$this->values = json_decode(file_get_contents($fileName),true);
	}

	public function get($key){
		echo "GET\n";
		clearstatcache();
		$modTime = filemtime($this->fileName);
		if($this->modificationTime < $modTime){
			$this->modificationTime = $modTime;
			$this->values = json_decode(file_get_contents($this->fileName),true);
			echo "LOAD\n";
		}
		return $this->values[$key];
	}

	public function set($key,$value){
		echo "SET\n";
		$this->values[$key] = $value;
		file_put_contents($this->fileName,json_encode($this->values));
		$this->modificationTime = time();
	}

}

$cachedFile = new CachedFile("cache.json");
$cachedFile->set('teste','o.O');
$cachedFile->get('teste');
sleep(5);
$cachedFile->get('teste');


