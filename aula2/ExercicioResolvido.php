<?php

class Entrada {

	private $file;
	private $columns;
	private $next;

	public function __construct($inputFile){
		$this->file = fopen($inputFile,'r');
		$columns = fgets($this->file);
		$this->columns = array_map('trim',explode('||',$columns));
	}

	public function __destruct(){
		fclose($this->file);
	}

	private function prepareNext(){
		$line = fgets($this->file);
		if($line === false){
			$this->next = false;
			return;
		}
		$values = array_map('trim',explode('||',$line));
		$this->next = array_combine($this->columns,$values);
	}

	/**
	* Deve ler uma linha do arquivo
	* e fazer um parse para array
	* array chave => valor
	*/
	public function getNext(){
		if($this->next === null){
			$this->prepareNext();
		}
		$next = $this->next;
		$this->next = null;
		return $next;
	}

	/**
	* retorna false se encontrou o fim do arquvio
	*/
	public function hasNext(){
		if($this->next === null){
			$this->prepareNext();
		}
		return $this->next !== false;
	}

}

class Saida {

	private $file;

	public function __construct($outputFile){
		$this->file = fopen($outputFile,'w');
		fprintf($this->file,'<pessoas>'."\n");
	}

	public function __destruct(){
		fprintf($this->file,'</pessoas>');
		fclose($this->file);
	}

	/**
	* Deve receber um array php
	* e escrever uma linha no arquivo
	*/
	public function putNext(Array $line){
		fprintf($this->file,"\t".'<pessoa');
		foreach ($line as $key => $value) {
			fprintf($this->file,' %s="%s"',$key,$value);
		}
		fprintf($this->file,' ></pessoa>'."\n");
	}

}

class Conversor{

	public $entrada;
	public $saida;

	public function __construct(Entrada $entrada,Saida $saida){
		$this->entrada = $entrada;
		$this->saida = $saida;
	}

	public function convert(){
		while($this->entrada->hasNext()){
			$next = $this->entrada->getNext();
			$this->saida->putNext($next);
		}
	}

}

$entrada = new Entrada("table.txt");
$saida = new Saida("table.xml");
$conversor = new Conversor($entrada,$saida);
$conversor->convert();
