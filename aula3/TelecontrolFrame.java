import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JList;
import javax.swing.DefaultListModel;


public class TelecontrolFrame {

	public static void main(String... args){
		JFrame frame = new JFrame("Aula 3 - DefaultListModel");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());

		DefaultListModel<String> modelList = new DefaultListModel<>();
		for(String string : args){
			modelList.addElement(string);
		}

		JList<String> viewList = new JList<>(modelList);
		frame.add(viewList,BorderLayout.CENTER);


		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

}