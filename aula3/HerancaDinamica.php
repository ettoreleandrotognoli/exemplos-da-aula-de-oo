<?php

interface Logger{

	public function logInfo($message);

	public function logError($message);

	public function logWarn($message);

}

class HtmlLogger implements Logger{

	public function logInfo($message){
		echo '<div class="log info">',$message,'</div>';
	}

	public function logError($message){
		echo '<div class="log error">',$message,'</div>';
	}

	public function logWarn($message){
		echo '<div class="log warn">',$message,'</div>';
	}
}

class FileLogger implements Logger{

	public $fileName;

	public function __construct($fileName){
		$this->fileName = $fileName;
	}

	public function logInfo($message){
		$file = fopen($this->fileName,'a');
		fwrite($file,'INFO ('.$this->currentTime().'):'.$message."\n");
		fclose($file);
	}

	public function logError($message){
		$file = fopen($this->fileName,'a');
		fwrite($file,'ERROR ('.$this->currentTime().'):'.$message."\n");
		fclose($file);
	}	

	public function logWarn($message){
		$file = fopen($this->fileName,'a');
		fwrite($file,'WARN ('.$this->currentTime().'):'.$message."\n");
		fclose($file);
	}

	protected function currentTime(){
		$dateTime = new DateTime();
		return $dateTime->format(DateTime::ISO8601);
	}

}

class Log implements Logger {

	public $infoLogger = array();

	public $errorLogger = array();

	public $warnLogger = array();

	public function logInfo($message){
		$loggers = $this->infoLogger;
		if(!is_array($loggers)){
			$loggers = array($loggers);
		}
		foreach ($loggers as $logger) {
			$logger->logInfo($message);
		}
	}

	public function logError($message){
		$loggers = $this->errorLogger;
		if(!is_array($loggers)){
			$loggers = array($loggers);
		}
		foreach ($loggers as $logger) {
			$logger->logError($message);
		}
	}

	public function logWarn($message){
		$loggers = $this->warnLogger;
		if(!is_array($loggers)){
			$loggers = array($loggerggers);
		}
		foreach ($loggers as $logger) {
			$logger->logWarn($message);
		}
	}

	public function addLogger(Logger $log){
		$this->warnLogger[] = $log;
		$this->infoLogger[] = $log;
		$this->errorLogger[] = $log;
	}

}

$htmlLogger = new HtmlLogger();
$fileLogger = new FileLogger('log.txt');
$log = new Log();
$log->addLogger($fileLogger);
$log->errorLogger[] = $htmlLogger;
$log->logWarn('Teste');
$log->logInfo('Teste');
$log->logError('Teste');