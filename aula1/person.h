#ifndef __PERSON__
#define __PERSON__

#include <stdio.h>

typedef struct {
	char name[100];
	char nickname[50];
	char momName[100];
	float monthlyPay;
} Person;

void personRead(Person * person){
	printf("Qual é seu nome?\n");
	scanf("%s",person->name);
	printf("Qual é seu apelido?\n");
	scanf("%s",person->nickname);
	printf("Qual o nome da sua mãe?\n");
	scanf("%s",person->momName);
	printf("Qual seu salário mensal?\n");
	scanf("%f",&(person->monthlyPay));
}

void personReadAndConfirm(Person * person){
	char response;
	do{
		personRead(person);
		printf("Então você se chama %s, sua mãe se chama %s, mas posso lhe chamar de %s\n",person->name,person->momName,person->nickname);		
		scanf(" %c",&response);
	}
	while(response != 's');
}

void personPrint(Person * person, float tax){
	printf("Você recebe R$%.2f ao ano\n",person->monthlyPay*12.0f);
	printf("Desconto de R$%.2f de imposto\n",person->monthlyPay*12.0f*tax);
}

void personWriteJSON(Person * person,FILE * file){
	fprintf(file,"{name:\"%s\",nickname:\"%s\",momName:\"%s\",monthlyPay:%f}\n",person->name,person->nickname,person->momName,person->monthlyPay);
}

#endif